#Hold Man Example App


This application demonstrates the hold / unhold capabilities of CafeX's FCSDK.

To get it up and running, follow these steps:

1. Clone the repo locally
2. Run the files on a PHP ready webserver (MAMP / WAMP)
3. Seetup your CafeX FCSDK server
4. Edit the source files (1001, 1002 index.html/session.php and the js file) replaceing server reference IPs with your own IP
5. Add the web app id reference in the session.php files to your web gateway (or use one of your own)

