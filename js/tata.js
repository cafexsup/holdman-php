<!-- hook up the hangup button -->
        $('#hangup').click(function() {
            
			// if we're on a call - end it!
            var call = UC.phone.getCalls()[0];
            if (call) {
                call.end();
				 alert('Ended call')
            }
            else { alert('Not on a call');}
        });

        function makeCall(number) {
            
  
            var number = number;
            var call = UC.phone.createCall(number);

            call.onRemoteMediaStream = function(mediaStream) {
                var elem = document.getElementById('remote');
                attachMediaStream(elem, mediaStream);
				
				
            };
			
			call.onLocalMediaStream = function(mediaStream) {
                var elem = document.getElementById('local');
                attachMediaStream(elem, mediaStream);
				
				
            }
			

			/// Muting ////

			$('#mute').click(function() {
					// if we're on a call - end it!
					call.enableLocalAudio(true)
					call.enableLocalVideo(true);
					
				
				});
				
		
			
				$('#unmute').click(function() {
					// if we're on a call - end it!
					call.enableLocalAudio(false);
					call.enableLocalVideo(false);
				});
				
		
		
			
			
			
			
		

            call.dial(true, true);
			
			var ring = new Audio('../telephone-ring-04.mp3');
			ring.play();
			
				
			call.onInCall = function() {
				ring.pause();
			};


			/// holding ///

			call.onRemoteHeld = function () {
			
				alert('on hold');
				$('#remote').hide();
				$('#holdman').show();
			};

			call.onRemoteUnheld = function () {
			
				alert('off hold');
				$('#remote').show();
				$('#holdman').hide();
			};
			
			$('#hold').click(function() {
					// if we're on a call - end it!
					call.hold();	
			});


			$('#unhold').click(function() {
					// if we're on a call - end it!
					call.resume();
					console.log('unheld');
			});
			
			
        }
		
	
		


 

        $.post('session.php', function (sessionID) {

            UC.start(sessionID, []);  
			

				var ring = new Audio('../telephone-ring-04.mp3');
	
			
			var call;
			UC.phone.onIncomingCall = function(newCall) {
				//Specify where preview and remote video should be played/presented.
				//UC.phone.setPreviewElement(document.getElementById('local'));
				//UC.phone.setVideoElement(document.getElementById('remote'));
				
	
				ring.play();
				
				var response = confirm("Call from: " + newCall.getRemoteAddress() +
				" - Would you like to answer?");
				if (response === true) {
					
				ring.pause();
				//What to do when the remote party ends the call
				newCall.onEnded = function() {
				alert("Call Ended");
				};
				//Remember the call to enable ending later
				call = newCall;
				
				
				
					call.onRemoteMediaStream = function(mediaStream) {
						var elem = document.getElementById('remote');
						attachMediaStream(elem, mediaStream);
					};
					
					 call.onLocalMediaStream = function(mediaStream) {
						var elem = document.getElementById('local');
						attachMediaStream(elem, mediaStream);
					};
				
				//Answer
				newCall.answer();

				/// Holding ////
				
						
				$('#hold').click(function() {
					// if we're on a call - end it!
					call.hold();
					
				
				});
				
				call.onRemoteHeld = function () {
			
					alert('on hold');
					$('#remote').hide();
					$('#holdman').show();
				};

				call.onRemoteUnheld = function () {
			
					alert('off hold');
					$('#remote').show();
					$('#holdman').hide();
				};
			
				$('#unhold').click(function() {
					// if we're on a call - end it!
					call.resume();
					console.log('unheld');	
				});
		
			/// Muting ////

			$('#mute').click(function() {
					// if we're on a call - end it!
					call.enableLocalAudio(true)
					call.enableLocalVideo(true);
					
				
				});
				
		
			
				$('#unmute').click(function() {
					// if we're on a call - end it!
					call.enableLocalAudio(false);
					call.enableLocalVideo(false);
				});
				
			
				
				} else {
				//Reject the call
				newCall.end();
				}
				
						
			 
			};

				
        })
        .fail(function (response) {
            alert('Failed to get session :-(');
        });